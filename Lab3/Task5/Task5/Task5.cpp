﻿// Уткин Дмитрий Сергеевич, 3 группа
// Лабораторная работа №3
// Задание №5 Вариант №24
// Вычислить за 5 операции a^23.

#include <iostream>
using namespace std;

void main()
{
	setlocale(LC_ALL, "russian");
	unsigned long long a, b, c, d, e, f, g, h;
	cout << "Введите число a: ";
	cin >> a;
	b = (a * a); // a^2
	c = (b * b); // a^4
	d = (c * c); // a^8
	e = d;
	f = (d * d); // a^16
	g = (f * d); // a^24
	h = (g / a); // a^23
	cout << h;
}