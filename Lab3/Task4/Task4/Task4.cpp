﻿// Уткин Дмитрий Сергеевич, 3 группа
// Лабораторная работа №3
// Задание №4 Вариант №9
// Вычислить за 6 операции a^64.

#include <iostream>
using namespace std;

void main()
{
	setlocale(LC_ALL, "russian");
	unsigned long long a, b, c, d, e, f, g;
	cout << "Введите число a. \n";
	cin >> a;
	b = (a * a); // a^2
	c = (b * b); // a^4
	d = (c * c); // a^8
	e = (d * d); // a^16
	f = (e * e); // a^32
	g = (f * f); // a^64
	cout << g;
}


