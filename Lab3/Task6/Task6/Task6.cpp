﻿// Уткин Дмитрий Сергеевич, 3 группа
// Лабораторная работа №3
// Задание №6 Вариант №23
// Найти значение y = f(x, t).Определить область допустимых значений для x и t.

#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
using namespace std;

int main()
{
	setlocale(LC_ALL, "russian");
	double f, x, t;
	cout << "Введите x: ";
	cin >> x;
	cout << "Введите t: ";
	cin >> t;
	if ((t * t * t * x + sqrt(1 + (sin(x * t) * sin(x * t)) + x * t * (cos(x * t) * cos(x * t))) > 0))
	f = (1 / (1 + x * x) - x * t) / (t * t * t * x + sqrt(1 + (sin(x*t) * sin(x*t)) + x * t * (cos(x*t) * cos(x*t))));
	else cout << "Не працює";
	cout << f;
	return 0;
}