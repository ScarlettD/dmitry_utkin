﻿// Уткин Дмитрий Сергеевич, 3 группа 
// Лабораторная работа №3
// Задание №2 Вариант №17
// Вычислить площадь круга, зная длину окружности.

#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
using namespace std;

int main()
{
	setlocale(LC_ALL, "russian");
	double C, S;
    cout << "Введите длину окружности: ";
	cin >> C;
	S = (M_PI * (C / (2 * M_PI)) * (C / (2 * M_PI)));
	cout << "Площадь окружности: " << S;
	return 0;
}
