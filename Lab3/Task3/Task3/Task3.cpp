﻿// Уткин Дмитрий Сергеевич, 3 группа
// Лабораторная работа №3
// Задание №3 Вариант №31
// Вычислить площадь кольца, зная длины внутренней и внешней окружностей.

#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
using namespace std;

int main()
{
	setlocale(LC_ALL, "russian");
	double C, D, E, F, S;
	cout << "Введите длину внешней окружности: ";
	cin >> C;
	cout << "Введите длину внутренней окружности: ";
	cin >> D;
	E = (M_PI * (C / (2 * M_PI)) * (C / (2 * M_PI)));
	F = (M_PI * (D / (2 * M_PI)) * (D / (2 * M_PI)));
	S = (E - F);
	cout << "Площадь кольца: " << S << endl;
	return 0;
}