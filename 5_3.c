/**
 * ...
 */
#include <stdio.h>
#include <stdint.h>
#include <stdio.h>

int main(void)
{
	uint16_t n;
	int32_t prev, prev_prev, cur;
	printf("Введите размерность последовательности: ");
	scanf("%hu", &n);
	printf("Начинайте вводить последовательность...\n");

	scanf("%d", &prev_prev);
	scanf("%d", &prev);
	--n, --n;

	uint16_t total_max = 0;
	while (n--) {
		scanf("%d", &cur);

		if ((prev > prev_prev) && (prev > cur))
			++total_max;

		prev_prev = prev;
		prev = cur;
	}

	printf("Чисел, которые больше своих соседей — %hu\n", total_max);

	return 0;
}
