﻿// Уткин Дмитрий Сергеевич, 3 группа
// Лабораторная работа №5
// Задание №2 Вариант №23
// Вычислить значение выражения
// S=7x + 7x^3/3 + 7x^5/5 + ... + 7x^(2n+1)/(2n+1)

#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "RUS");
	double x, n, S;
	cout << "Введите x и n: ";
	cin >> x >> n;
	S = 0;
	for (int i = 0; i <= n; i++)
	{
		S += (7 * pow(x, (2 * i + 1))) / (2 * i + 1);
	}
	cout << S;
	return 0;
}