﻿// Уткин Дмитрий Сергеевич, 3 группа
// Лабораторная работа №5
// Задание №1
// Напечатать числа в виде следующей таблицы:
// 1 2 3 4 ... N
// ...
// 1 2 3 4
// 1 2 3
// 1 2
// 1

#include "pch.h"
#include <stdio.h>
#include <stdint.h>
#include <iostream>
using namespace std;


/* 

int main()
{
	setlocale(LC_ALL, "RUS");
	int N;
	cout << "Введите N: ";
	cin >> N;
	for (N = N; N >= 1; N--)
	{
		for (int i = 1; i <= N; i++)
		{
			cout << i << " ";
		}
		cout << endl;
	}
	return 0;
}


*/

/* 

// Уткин Дмитрий Сергеевич, 3 группа
// Лабораторная работа №5
// Задание №2 Вариант №23
// Вычислить значение выражения
// S=7x + 7x^3/3 + 7x^5/5 + ... + 7x^(2n+1)/(2n+1)


int main()
{
	setlocale(LC_ALL, "RUS");
	double x, n, S;
	cout << "Введите x и n: ";
	cin >> x >> n;
	S = 0;
	for (int i = 0; i <= n; i++)
	{
		S += (7 * pow(x, (2 * i + 1))) / (2 * i + 1);
	}
	cout << S;
	return 0;
}

*/



int main()
{
	setlocale(LC_ALL, "RUS");
	int n;
	int prev, prev_prev, cur;
	cout << "Введите длину последовательности: ";
	cin >> n;
	cout << "Введите " << n << " элементов последовательности: " << endl;
	cin >> prev_prev;
	cin >> prev;
	n = n - 2;

	int total_max = 0;
	for (int i = 0; i < n; i++) {
		cin >> cur;

		if ((prev > prev_prev) && (prev > cur))
			total_max++;

		prev_prev = prev;
		prev = cur;
	}
	cout << "Чисел, которые больше своих соседей: " << total_max;
	return 0;
}


int main()
{
	setlocale(LC_ALL, "RUS");
	int N;
	cout << "Введите длину последовательности: ";
	cin >> N;
	for (int i = 0; i < N; i++)
		cout << "Введите " << i << " элемент последовательности: ";
	cin >> 