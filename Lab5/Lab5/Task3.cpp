﻿//Уткин Дмитрий Сергеевич, 3 группа
//Лабораторная работа №5
//Задание №3 Вариант №7
//Дана непустая последовательность чисел, длина последовательности N.
//Определить, сколько из них больше своих соседей, т.е. предыдущего и последующего чисел.

#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "RUS");
	int n;
	int prev, prev_prev, current;
	cout << "Введите размерность последовательности: ";
	cin >> n;
	cout << "Введите последовательность: ";
	cin >> prev_prev;
	cin >> prev;
	n -= 2;
	int total_max = 0;
	while (n--)
	{
		cin >> current;
		if ((prev > prev_prev) && (prev > current))
			++total_max;
		prev_prev = prev;
		prev = current;
	} 
	cout << "Больше своих соседей: " << total_max;
	return 0;
}