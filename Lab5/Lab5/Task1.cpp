﻿// Уткин Дмитрий Сергеевич, 3 группа
// Лабораторная работа №5
// Задание №1
// Напечатать числа в виде следующей таблицы:
// 1 2 3 4 ... N
// ...
// 1 2 3 4
// 1 2 3
// 1 2
// 1

#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "RUS");
	int N;
	cout << "Введите N: ";
	cin >> N;
	for (N = N; N >= 1; N--)
	{
		for (int i = 1; i <= N; i++)
		{
			cout << i << " ";
		}
		cout << endl;
	}
	return 0;
}