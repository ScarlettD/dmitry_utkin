﻿// Уткин Дмитрий Сергеевич, 3 группа
// Лабораторная работа №4
// Задание №9 Вариант №3
/*
Составить программу, которая в зависимости от порядкового номера дня
месяца (1, 2, ..., 12) выводит на экран время года, к которому относится
этот месяц.
*/

#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "russian");
	int A;
	cout << "Введите порядковый номер месяца: ";
	cin >> A;
	switch (A)
	{
	case 1: { cout << "Зима" << endl; break; }
	case 2: { cout << "Зима" << endl; break; }
	case 3: { cout << "Весна" << endl; break; }
	case 4: { cout << "Весна" << endl; break; }
	case 5: { cout << "Весна" << endl; break; }
	case 6: { cout << "Лето" << endl; break; }
	case 7: { cout << "Лето" << endl; break; }
	case 8: { cout << "Лето" << endl; break; }
	case 9: { cout << "Осень" << endl; break; }
	case 10: { cout << "Осень" << endl; break; }
	case 11: { cout << "Осень" << endl; break; }
	case 12: { cout << "Зима" << endl; break; }
	}
	return 0;
}