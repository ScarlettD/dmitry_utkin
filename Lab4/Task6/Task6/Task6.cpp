﻿// Уткин Дмитрий Сергеевич, 3 группа
// Лабораторная работа №4
// Задание №6 Вариант №6
// Для вещественных x,y,z вычислить min((x+y+z)/3, xy/(z*z))+27.

#include <iostream>
#define min(x, y) x<y ? x : y
using namespace std;

void main()
{
	setlocale(LC_ALL, "russian");
	float x, y, z;
	cout << "Введите x, y, z: ";
	cin >> x >> y >> z;
	float min((x + y + z) / 3, x*y / (z * z)) + 27;
		
}
