﻿// Уткин Дмитрий Сергеевич, 3 группа
// Лабораторная работа №4
// Задание №10
/*
В восточном календаре принят 60-летний цикл,
состоящий из 12-летних подциклов, обозначаемых названиями цвета:
зеленый, красный, желтый, белый и голубой. При этом каждый цвет следует
по два года подряд. В каждом подцикле годы носят названия животных:
крысы, коровы, тигра, зайца, дракона, змеи, лошади, овцы, обезьяны, петуха,
собаки и свиньи. По номеру года определить его название, если 4 год нашей
эры — начало цикла: «год зеленой крысы».
*/

#include "pch.h"
#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "russian");
	int year;
	cout << "Введите год: ";
	cin >> year;
	year -= 4;
	cout << "Это год ";
	switch (year % 10)
	{
	case -4: cout << "бело"; break;
	case -3: cout << "бело"; break;
	case -2: cout << "голубо"; break;
	case -1: cout << "голубо"; break;
	case 0: cout << "зелено"; break;
	case 1: cout << "зелено"; break;
	case 2: cout << "красно"; break;
	case 3: cout << "красно"; break;
	case 4: cout << "желто"; break;
	case 5: cout << "желто"; break;
	case 6: cout << "бело"; break;
	case 7: cout << "бело"; break;
	case 8: cout << "голубо"; break;
	case 9: cout << "голубо"; break;
	}
	switch (year % 12)
	{
	case -4: cout << "й обезьяны." << endl; break;
	case -3: cout << "го петуха." << endl; break;
	case -2: cout << "й собаки." << endl; break;
	case -1: cout << "й свиньи." << endl; break;
	case 0: cout << "й крысы." << endl; break;
	case 1: cout << "й коровы." << endl; break;
	case 2: cout << "го тигра." << endl; break;
	case 3: cout << "го зайца." << endl; break;
	case 4: cout << "го дракона." << endl; break;
	case 5: cout << "й змеи." << endl; break;
	case 6: cout << "й лошади." << endl; break;
	case 7: cout << "й овцы." << endl; break;
	case 8: cout << "й обезьяны." << endl; break;
	case 9: cout << "го петуха." << endl; break;
	case 10: cout << "й собаки." << endl; break;
	case 11: cout << "й свиньи." << endl; break;
	}
	return 0;
}
