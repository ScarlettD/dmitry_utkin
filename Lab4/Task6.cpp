﻿// Уткин Дмитрий Сергеевич, 3 группа
// Лабораторная работа №4
// Задание №6 Вариант №6
// Для вещественных x,y,z вычислить min((x+y+z)/3, xy/(z*z))+27.

#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "russian");
	double x, y, z;
	cout << "Введите x, y, z: ";
	cin >> x >> y >> z;
	if (((x + y + z) / 3) > (x* y / (z * z)))
		cout << (x * y / (z * z)) + 27 << endl;
	else cout << ((x + y + z) / 3) + 27 << endl;
	return 0;
}
