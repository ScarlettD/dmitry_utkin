﻿// Уткин Дмитрий Сергеевич, 3 группа
// Лабораторная работа №4
// Задание №2
// Напишите программу: даны произвольные числа a, b, c. Если нельзя построить треугольник с такими длинами сторон - напечатайте 0,
// иначе напечатать 3, 2, 1 в зависимости от того, равносторонний это треугольник, равнобедренный или какой-либо иной.

#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "russian");
	double a, b, c;
	cout << "Введите a: ";
	cin >> a;
	cout << "Введите b: ";
	cin >> b;
	cout << "Введите c: ";
	cin >> c;
	if (a > b + c || b > a + c || c > a + b)
		cout << 0;
	else if (a == b && b == c && a == c)
			cout << 3;
    else if (a == b && a != c || b == c && b != a || a == c && a != b)
		    cout << 2;
	else cout << 1;
	return 0;
}